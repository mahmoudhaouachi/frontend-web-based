


var params = (new URL(document.location)).searchParams;
var id = params.get("id");

// Create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest();
// Open a new connection, using the GET request on the URL endpoint
request.open('GET', 'https://test-frontend-neppo.herokuapp.com/pessoas/'+id+'.json', true);

request.onload = function () {
	
// Begin accessing JSON data here
var person = JSON.parse(this.response);

if (request.status >= 200 && request.status < 400) {
  
	console.log(person.nome);

  // Create an td and set the text content to the person's name
  document.getElementById('name').value = person.nome
  // Create an td and set the text content to the person's name
   document.getElementById('birth').value = person.dataNascimento;
  // Create an td and set the text content to the person's name
  document.getElementById('doc').value = person.identificacao;
 //
 if (person.sexo === 'Male') {
 	  document.getElementById('male').checked = true;
 } else {
 	 	  document.getElementById('female').checked = true;

 }
   // Create an td and set the text content to the person's name
  document.getElementById('address').value = person.endereco;

}

}
// Send request
request.send();





function editPerson(){

var req = new XMLHttpRequest();
req.open("PUT", "https://test-frontend-neppo.herokuapp.com/pessoas/"+id,true);
req.setRequestHeader("Content-type","application/json; charset=utf-8");

var name = document.getElementById("name").value;
var birth = document.getElementById("birth").value;
var doc = document.getElementById("doc").value;
var sexs = document.getElementsByName("sex");

for (var i = 0, length = sexs.length; i < length; i++)
{
 if (sexs[i].checked)
 {
  // do whatever you want with the checked radio
  var sex = sexs[i].value;
  // only one radio can be logically checked, don't check the rest
  break;
 }
}
var address = document.getElementById("address").value;


var input = JSON.stringify({
"nome": name,
"dataNascimento" : birth,
"identificacao": doc,
"sexo" : sex,
"endereco" : address});

req.send(input);


 setTimeout("alert('person edited');",1000);

 setTimeout("window.location.replace('index.html');",1000);

}