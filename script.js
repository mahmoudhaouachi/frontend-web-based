

const app = document.getElementById('root');
//
const table = document.createElement('table');
table.setAttribute('class', 'table');

//
const thead = document.createElement('thead');
//
const trh = document.createElement('tr');
//
const thname = document.createElement('th');
thname.textContent = 'Name';
//
const thbirth = document.createElement('th');
thbirth.textContent = 'Birthdate';
//
const thdoc = document.createElement('th');
thdoc.textContent = 'Document ID';
//
const thsex = document.createElement('th');
thsex.textContent = 'Sex';
//
const thadd = document.createElement('th');
thadd.textContent = 'Address';
//
const tbody = document.createElement('tbody');



app.appendChild(table);
table.appendChild(thead);
table.appendChild(tbody);
thead.appendChild(trh);
trh.appendChild(thname);
trh.appendChild(thbirth);
trh.appendChild(thdoc);
trh.appendChild(thsex);
trh.appendChild(thadd);







// Create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest();
// Open a new connection, using the GET request on the URL endpoint
request.open('GET', 'https://test-frontend-neppo.herokuapp.com/pessoas.json', true);


request.onload = function () {
	
// Begin accessing JSON data here
var data = JSON.parse(this.response);

if (request.status >= 200 && request.status < 400) {
  data.forEach( person => {
const tr = document.createElement('tr');
  // Create an td and set the text content to the person's name
  const name = document.createElement('td');
  name.textContent = person.nome;
  // Create an td and set the text content to the person's name
  const birth = document.createElement('td');
  birth.textContent = person.dataNascimento;
  // Create an td and set the text content to the person's name
  const doc = document.createElement('td');
  doc.textContent = person.identificacao;
  // Create an td and set the text content to the person's name
  const sex = document.createElement('td');
  sex.textContent = person.sexo;
   // Create an td and set the text content to the person's name
  const add = document.createElement('td');
  add.textContent = person.endereco;

// Create an td and set the text content to the person's name
const detail = document.createElement('td');
const show =document.createElement('a'); 
show.textContent = "Show";
show.setAttribute('id', person.id);
show.setAttribute('href', 'detail.html?id='+ person.id);
// Create an td and set the text content to the person's name
const del = document.createElement('td');
const delet = document.createElement('a'); 
delet.textContent = "Delete";
delet.setAttribute('id', person.id);
delet.setAttribute('href', '#');
delet.setAttribute('onClick', 'delPerson(id)');
// Create an td and set the text content to the person's name
const update = document.createElement('td');
const edit = document.createElement('a'); 
edit.textContent = "Edit";
edit.setAttribute('id', person.id);
edit.setAttribute('href', 'edit.html?id='+person.id);


    tbody.appendChild(tr);
    tr.appendChild(name);
    tr.appendChild(birth);
    tr.appendChild(doc);
    tr.appendChild(sex);
    tr.appendChild(add);
    tr.appendChild(detail);
    detail.appendChild(show);
    tr.appendChild(del);
    del.appendChild(delet);
    tr.appendChild(update);
    update.appendChild(edit);
  });
} else {
  console.log('error');
}

}
// Send request
request.send();



function delPerson(id) {

if (confirm("Are you sure ?"))
{	
var req = new XMLHttpRequest();
req.open("DELETE","https://test-frontend-neppo.herokuapp.com/pessoas/"+id, true);

req.send(null);

setTimeout("alert('person was deleted successfully !');", 1000);

setTimeout("location.reload(true);",1000);}

}

